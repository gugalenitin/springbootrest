package com.myappdirect.myapi.repository;

import com.myappdirect.myapi.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource
public interface UserRepository extends CrudRepository<User, Long> {
}
