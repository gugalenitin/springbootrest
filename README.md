# Simple Spring Boot Rest Resource App #
## 1.0 ##

This app uses ```@RestResource``` from ```spring-boot-starter-data-rest``` to create REST API. We only need to create a model and repository and everything else will be handled by Spring Boot.


## Requirements ##

* Java 8
* Maven

## How to run? ##

* Run API - ```mvn spring-boot:run```